const express = require('express')
require('dotenv').config()
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')

mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to the database'))

const app = express()
app.use(cors())

app.use(express.json());

const router = require('./routes')
app.use(router)

app.listen(3000, () => console.log('Server is starting..'))