const User = require("../models/user");

module.exports = {
  checkUser: async (req, res, next) => {
    try {
      const user = await User.findById(req.params.id);
      if (!user) {
        return res.status(404).json({ message: "User not found" });
      }
      req.existingUser = user;
      next();
    } catch (error) {
      res.status(500).json({
        message: "Internal server error occurred",
        error: error.message,
      });
    }
  },
};
