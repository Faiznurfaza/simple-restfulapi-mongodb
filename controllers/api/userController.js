const User = require("../../models/user");
const bcrypt = require("bcrypt");
const saltRounds = 10;

module.exports = {
  getAllUser: async (req, res) => {
    try {
      const users = await User.find();

      return res.status(200).json({
        status: "Success",
        message: "Get all users success",
        users: users,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal server error",
        error: error.message,
      });
    }
  },
  getUserById: async (req, res) => {
    try {
      const user = req.existingUser;
      res.status(200).json({
        message: "Get user by id success",
        user: user,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  createUser: async (req, res) => {
    const { name, email, password } = req.body;
    try {
      const existingUser = await User.findOne({ $or: [{ name }, { email }] });

      if (existingUser) {
        return res.status(409).json({
          message: "User with the same name or email already exists",
        });
      }
      const hashedPassword = await bcrypt.hash(password, saltRounds);

      const user = new User({
        name: name,
        email: email,
        password: hashedPassword,
      });
      const newUser = await user.save();
      res.status(201).json({
        message: "New user created",
        user: newUser,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal server error",
        error: error.message,
      });
    }
  },
  updateUser: async (req, res) => {
    const { name, email, password } = req.body;
    const user = req.existingUser;
    if (name != null) {
      user.name = name;
    }
    if (email != null) {
      user.email = email;
    }
    if (password != null) {
      const newPassword = await bcrypt.hash(password, saltRounds);
      user.password = newPassword;
    }
    try {
      const updatedUser = await user.save();
      res.status(200).json({ message: "User updated", user: updatedUser });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },
  deleteUser: async (req, res) => {
    try {
      const user = req.existingUser;
      await user.deleteOne();

      res.status(200).json({
        message: "User deleted",
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
};
