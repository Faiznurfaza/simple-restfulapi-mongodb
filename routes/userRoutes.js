const userController = require('../controllers/api/userController')

const userCheck = require('../middlewares/userCheck')

const router = require('express').Router()

router.get('/', userController.getAllUser)
router.get('/:id', userCheck.checkUser, userController.getUserById)
router.post('/register', userController.createUser)
router.patch('/:id', userCheck.checkUser, userController.updateUser)
router.delete('/:id', userCheck.checkUser, userController.deleteUser)


module.exports = router